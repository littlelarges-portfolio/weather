import 'package:flutter/material.dart';
import '../../presentation/widgets/home_page/rounded_weather_info_card.dart';

class AppData {
  List<RoundedWeatherCardInfo> roundedWeatherCardInfos = [
    RoundedWeatherCardInfo(
        time: '1 AM', temperature: '14°', image: WeatherImages.moonCloudFastWind, color: RoundedWeatherInfoCardColors.past, isCurrent: false),
    RoundedWeatherCardInfo(
        time: 'NOW', temperature: '18°', image: WeatherImages.moonCloudFastWind, color: RoundedWeatherInfoCardColors.current, isCurrent: true),
    RoundedWeatherCardInfo(
        time: '3 AM', temperature: '19°', image: WeatherImages.moonCloudMidRain, color: RoundedWeatherInfoCardColors.future, isCurrent: false),
    RoundedWeatherCardInfo(
        time: '5 AM', temperature: '17°', image: WeatherImages.sunCloudMidRain, color: RoundedWeatherInfoCardColors.future, isCurrent: false),
    RoundedWeatherCardInfo(
        time: '6 AM', temperature: '22°', image: WeatherImages.moonCloudMidRain, color: RoundedWeatherInfoCardColors.future, isCurrent: false),
    RoundedWeatherCardInfo(
        time: '7 AM', temperature: '17°', image: WeatherImages.moonCloudFastWind, color: RoundedWeatherInfoCardColors.future, isCurrent: false),
    RoundedWeatherCardInfo(
        time: '8 AM', temperature: '16°', image: WeatherImages.sunCloudRain, color: RoundedWeatherInfoCardColors.future, isCurrent: false),
    RoundedWeatherCardInfo(
        time: '9 AM', temperature: '17°', image: WeatherImages.moonCloudFastWind, color: RoundedWeatherInfoCardColors.future, isCurrent: false),
    RoundedWeatherCardInfo(
        time: '10 AM', temperature: '17°', image: WeatherImages.sunCloudMidRain, color: RoundedWeatherInfoCardColors.future, isCurrent: false),
    RoundedWeatherCardInfo(
        time: '11 AM', temperature: '18°', image: WeatherImages.moonCloudFastWind, color: RoundedWeatherInfoCardColors.future, isCurrent: false),
  ];
}

class RoundedWeatherCardInfo {
  RoundedWeatherCardInfo(
      {required this.time, required this.temperature, required this.image, required this.color, required this.isCurrent});

  String time, temperature;

  Image image;

  RoundedWeatherInfoCardColors color;

  bool isCurrent;
}

class AppImages {
}

class WeatherImages {
  static final Image moonCloudFastWind =
      Image.asset('assets/images/moon_cloud_fast_wind.png');
  static final Image moonCloudMidRain =
      Image.asset('assets/images/moon_cloud_mid_rain.png');
  static final Image sunCloudMidRain =
      Image.asset('assets/images/sun_cloud_mid_rain.png');
  static final Image sunCloudRain =
      Image.asset('assets/images/sun_cloud_mid_rain.png');
}
