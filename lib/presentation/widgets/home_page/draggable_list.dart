import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:weather_app/main.dart';
import 'package:weather_app/presentation/widgets/home_page/draggable_sheet_widget_card.dart';
import 'package:weather_app/presentation/widgets/home_page/rounded_weather_info_card.dart';

class DraggableList extends StatelessWidget {
  const DraggableList({required this.scrollController, super.key});

  final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        controller: scrollController,
        child: Padding(
          padding: const EdgeInsets.only(right: 20, left: 20),
          child: Column(
            children: [
              const SizedBox(height: 20),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height / 8,
                  child: ListView.separated(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) => RoundedWeatherInfoCard(
                          isCurrent:
                              appData.roundedWeatherCardInfos[index].isCurrent,
                          hexColor:
                              appData.roundedWeatherCardInfos[index].color,
                          time: appData.roundedWeatherCardInfos[index].time,
                          temperature: appData
                              .roundedWeatherCardInfos[index].temperature,
                          image: appData.roundedWeatherCardInfos[index].image),
                      separatorBuilder: (context, index) =>
                          const SizedBox(width: 10),
                      itemCount: appData.roundedWeatherCardInfos.length),
                ),
                const SizedBox(height: 18),
                StaggeredGrid.count(
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20,
                  crossAxisCount: 2,
                  children: [
                    StaggeredGridTile.count(
                      crossAxisCellCount: 2,
                      mainAxisCellCount: 1,
                      child: DraggableSheetWidgetCard(
                        title: 'AIR QUALITY',
                        icon: Icon(
                          Icons.snowing,
                          color: HexColor('#9392A8'),
                        ),
                      ),
                    ),
                    StaggeredGridTile.count(
                      crossAxisCellCount: 1,
                      mainAxisCellCount: 1,
                      child: DraggableSheetWidgetCard(
                          title: 'UV INDEX',
                          icon: Icon(
                            Icons.sunny,
                            color: HexColor('#9392A8'),
                          )),
                    ),
                    StaggeredGridTile.count(
                      crossAxisCellCount: 1,
                      mainAxisCellCount: 1,
                      child: DraggableSheetWidgetCard(
                        title: 'SUNRISE',
                        icon: Icon(
                          Icons.room_service_outlined,
                          color: HexColor('#9392A8'),
                        ),
                      ),
                    ),
                    StaggeredGridTile.count(
                      crossAxisCellCount: 1,
                      mainAxisCellCount: 1,
                      child: DraggableSheetWidgetCard(
                        title: 'SUNRISE',
                        icon: Icon(
                          Icons.water_outlined,
                          color: HexColor('#9392A8'),
                        ),
                      ),
                    ),
                    StaggeredGridTile.count(
                      crossAxisCellCount: 1,
                      mainAxisCellCount: 1,
                      child: DraggableSheetWidgetCard(
                        title: 'SUNRISE',
                        icon: Icon(
                          Icons.water_drop,
                          color: HexColor('#9392A8'),
                        ),
                      ),
                    ),
                    StaggeredGridTile.count(
                      crossAxisCellCount: 1,
                      mainAxisCellCount: 1,
                      child: DraggableSheetWidgetCard(
                        title: 'SUNRISE',
                        icon: Icon(
                          Icons.thermostat,
                          color: HexColor('#9392A8'),
                        ),
                      ),
                    ),
                    StaggeredGridTile.count(
                      crossAxisCellCount: 1,
                      mainAxisCellCount: 1,
                      child: DraggableSheetWidgetCard(
                        title: 'SUNRISE',
                        icon: Icon(
                          Icons.water_sharp,
                          color: HexColor('#9392A8'),
                        ),
                      ),
                    ),
                    StaggeredGridTile.count(
                      crossAxisCellCount: 1,
                      mainAxisCellCount: 1,
                      child: DraggableSheetWidgetCard(
                        title: 'SUNRISE',
                        icon: Icon(
                          Icons.visibility,
                          color: HexColor('#9392A8'),
                        ),
                      ),
                    ),
                    StaggeredGridTile.count(
                      crossAxisCellCount: 1,
                      mainAxisCellCount: 1,
                      child: DraggableSheetWidgetCard(
                        title: 'SUNRISE',
                        icon: Icon(
                          Icons.speed_outlined,
                          color: HexColor('#9392A8'),
                        ),
                      ),
                    ),
                  ],
                )
              ]),
            ],
          ),
        ),
      ),
    );
  }
}
