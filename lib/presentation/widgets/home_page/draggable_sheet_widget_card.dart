import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class DraggableSheetWidgetCard extends StatelessWidget {
  const DraggableSheetWidgetCard(
      {required this.title, required this.icon, this.content, super.key});

  final String title;
  final Icon icon;
  final Widget? content;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(18),
        height: 164,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(22)),
          color: HexColor('#272454'),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                icon,
                const SizedBox(width: 6),
                Text(title, style: TextStyle(color: HexColor('#9392A8'))),
              ],
            ),
            content ?? Container()
          ],
        ));
  }
}
