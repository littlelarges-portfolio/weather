import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:weather_app/logic/utilities/Utilities.dart';

class DraggableBar extends StatelessWidget {
  const DraggableBar({required this.scrollController, super.key});

  final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    return ScrollConfiguration(
      behavior: const ScrollBehavior().copyWith(overscroll: false),
      child: SingleChildScrollView(
        controller: scrollController,
        child: Stack(
          children: [
            Positioned(top: 0, right: 0, left: 0,child: Image.asset('assets/images/home_indicator.png')),
            Column(
              children: [
                Container(
                  height: 52,
                  decoration: BoxDecoration(
                      // color: Colors.red.withOpacity(.5),
                      border: Border.all(color: Colors.white, width: .2),
                      boxShadow: const [
                        BoxShadow(
                            blurStyle: BlurStyle.outer,
                            blurRadius: 1,
                            color: Colors.white,
                            offset: Offset(-.5, -2))
                      ],
                      borderRadius: const BorderRadius.only(
                          topRight: Radius.circular(44),
                          topLeft: Radius.circular(44))),
                  child: Stack(children: [
                    Positioned(
                        bottom: 0,
                        child: Image.asset('assets/images/underline.png')),
                  ]),
                ),
              ],
            ),
            Column(
              children: [
                const SizedBox(height: 25),
                Padding(
                  padding: const EdgeInsets.only(right: 32, left: 32),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Hourly Forecast',
                          style: TextStyle(
                              color: HexColor('#EBEBF5'),
                              fontSize: 15,
                              height: 20.0.toFigmaHeight(15))),
                      Text('Weekly Forecast',
                          style: TextStyle(
                              color: Colors.grey[400],
                              fontSize: 15,
                              height: 20.0.toFigmaHeight(15))),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
