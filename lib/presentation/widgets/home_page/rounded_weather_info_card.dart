import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class RoundedWeatherInfoCard extends StatelessWidget {
  RoundedWeatherInfoCard(
      {required this.isCurrent,
      required this.hexColor,
      required this.image,
      required this.time,
      required this.temperature,
      super.key}) {
    color = isCurrent
        ? HexColor(hexColor.value)
        : HexColor(hexColor.value).withOpacity(.2);
  }

  final bool isCurrent;
  final String time, temperature;
  final Image image;

  late final Color color;

  final RoundedWeatherInfoCardColors hexColor;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.all(Radius.circular(30)),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 50, sigmaY: 50),
        child: Container(
            width: 60,
            height: 146,
            decoration: BoxDecoration(
              boxShadow: const [
                BoxShadow(
                    blurStyle: BlurStyle.outer,
                    blurRadius: 1,
                    color: Colors.white,
                    offset: Offset(.5, 1))
              ],
              borderRadius: const BorderRadius.all(Radius.circular(30)),
              border: Border.all(width: .1, color: Colors.white),
              color: color,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(time,
                    style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 15)),
                image,
                Text(temperature,
                    style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.normal,
                        fontSize: 20)),
              ],
            )),
      ),
    );
  }
}

enum RoundedWeatherInfoCardColors {
  past('#7E319D'),
  current('#48319D'),
  future('#48319D');

  const RoundedWeatherInfoCardColors(this.value);
  final String value;
}
