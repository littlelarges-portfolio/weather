import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';

import '../../logic/utilities/Utilities.dart';
import '../widgets/home_page/draggable_bar.dart';
import '../widgets/home_page/draggable_list.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: Scaffold(
          body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                    'assets/images/backgrounds/main_background_image.jpg'),
                fit: BoxFit.fill)),
        child: SafeArea(
            child: Stack(alignment: Alignment.bottomCenter, children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 51,
              ),
              Text(
                'Montreal',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 34,
                  height: 47.0.toFigmaHeight(34),
                  letterSpacing: .37,
                ),
              ),
              const SizedBox(
                height: 12,
              ),
              Text(
                '19°',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 96,
                    height: 70.0.toFigmaHeight(96),
                    letterSpacing: .37,
                    fontWeight: FontWeight.w200),
              ),
              const SizedBox(
                height: 12,
              ),
              Column(
                children: [
                  SizedBox(
                    width: 120,
                    child: Column(
                      children: [
                        Text(
                          'Mostly Clear',
                          style: TextStyle(
                              color: HexColor('#EBEBF5'),
                              height: 24.0.toFigmaHeight(20),
                              fontSize: 20,
                              letterSpacing: .37,
                              fontWeight: FontWeight.w600),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'H:24°',
                              style: TextStyle(
                                  color: HexColor('#FFFFFF'),
                                  fontSize: 20,
                                  letterSpacing: .37,
                                  fontWeight: FontWeight.w600),
                            ),
                            Text(
                              'L:18°',
                              style: TextStyle(
                                  color: HexColor('#FFFFFF'),
                                  fontSize: 20,
                                  letterSpacing: .37,
                                  fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 23,
              ),
              Image.asset('assets/images/house.png'),
            ],
          ),
          DraggableScrollableSheet(
              initialChildSize: .35,
              minChildSize: .35,
              // maxChildSize: .85,
              builder: ((context, scrollController) {
                return ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(44),
                      topLeft: Radius.circular(44)),
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 50, sigmaY: 50),
                    child: Container(
                        // blur: 50,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(colors: [
                              HexColor('#2E335A').withOpacity(.26),
                              HexColor('#1C1B33').withOpacity(.26),
                            ]),
                            borderRadius: const BorderRadius.only(
                                topRight: Radius.circular(44),
                                topLeft: Radius.circular(44))),
                        child: Column(
                          children: [
                            DraggableBar(
                              scrollController: scrollController,
                            ),
                            DraggableList(
                              scrollController: scrollController,
                            ),
                          ],
                        )),
                  ),
                );
              })),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child:
                Image.asset('assets/images/bottom_bar.png', fit: BoxFit.fill),
          ),
          Positioned(
            bottom: 0,
            child: Image.asset('assets/images/subtract.png', fit: BoxFit.fill),
          ),
          Positioned(
            bottom: -12,
            child: Image.asset(
              'assets/images/button.png',
            ),
          ),
        ])),
      )),
    );
  }
}